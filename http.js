const http = require("http");
const uuid = require("uuid");
// to create an HTTP server
const myServer = http.createServer((req, res) => {
  if (req.url === "/html") {
    // to check if the requested URL is "/html"
    res.writeHead(200, { "Content-Type": "text/html" }); // to set the response headers for HTML content
    const htmlContent = `<!DOCTYPE html>
      <html>
        <head>
        </head>
        <body>
            <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
            <p> - Martin Fowler</p>
        </body>
      </html>`;

    res.end(htmlContent); // to send the HTML content as the response
  } else if (req.url === "/json") {
    // to check if the requested URL is "/json"
    res.writeHead(200, { "Content-Type": "application/json" });
    const jsonContent = {
      slideshow: {
        author: "Yours Truly",
        date: "date of publication",
        slides: [
          {
            title: "Wake up to WonderWidgets!",
            type: "all",
          },
          {
            items: [
              "Why <em>WonderWidgets</em> are great",
              "Who <em>buys</em> WonderWidgets",
            ],
            title: "Overview",
            type: "all",
          },
        ],
        title: "Sample Slide Show",
      },
    };
    res.end(JSON.stringify(jsonContent, null, 2)); // to send the JSON content as the response
  } else if (req.url === "/uuid") {
    // to check if the requested URL is "/uuid"
    res.writeHead(200, { "Content-Type": "application/json" });
    const uuidContent = { uuid: uuid.v4() }; //to generate uuid v4
    res.end(JSON.stringify(uuidContent, null, 2)); // to send the JSON content with the generated UUID
  }
  // to check if the requested URL starts with "/status/"
  else if (req.url.startsWith("/status/")) {
    const statusCode = Number(req.url.slice(8)); // to get the status code from path of url
    if (statusCode >= 100 && statusCode <= 599) {
      //to check if status code is in valid range
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end(`Return a response with ${statusCode} status code`); // to send the response with the specified status code
    } else {
      res.writeHead(400, { "Content-Type": "text/plain" });
      res.end(`Invalid status code`); // Send an error response
    }
  } else if (req.url.startsWith("/delay/")) {
    // tp check if the requested URL starts with "/delay/"
    const delay = Number(req.url.slice(7)); // to get the delay from path of url
    setTimeout(() => {
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end(`return a response with 200 status code`); // to send a response with a 200 status code after the specified delay
    }, delay * 1000);
  } else {
    res.writeHead(404, { "Content-Type": "text/plain" });
    res.end(`Not found`);
  }
});

const myPort = 7000; //Specify the port
myServer.listen(myPort, () => {
  //apply listen method on created server
  console.log(`Running smoothly at http://localhost:${myPort}`);
});
